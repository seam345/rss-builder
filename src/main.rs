use rss::ChannelBuilder;
use std::fs;
use std::io::Write;

const EXTENSION: &'static str = env!("EXTENSION");
const MIME_TYPE: &'static str = env!("MIME_TYPE");
const BASE_URL: &'static str = env!("BASE_URL");
const TITLE: &'static str = env!("TITLE");
const OUTPUT_FILE: &'static str = env!("OUTPUT_FILE");

fn main() {
    let mut items = Vec::new();

    for path in vec_of_extension_match().iter() {
        items.push(
            rss::ItemBuilder::default()
                .guid(Some(rss::Guid {
                    value: path
                        .file_name()
                        .expect("filename")
                        .to_str()
                        .unwrap()
                        .to_string(),
                    permalink: false,
                }))
                .title(Some(
                    path.file_stem()
                        .expect("filename")
                        .to_str()
                        .unwrap()
                        .to_string(),
                ))
                .link(Some(
                    BASE_URL.to_owned()
                        + &urlencoding::encode(
                            &path.file_name().expect("filename").to_str().unwrap(),
                        ),
                ))
                .enclosure(Some(rss::Enclosure {
                    url: BASE_URL.to_owned()
                        + &urlencoding::encode(
                            &path.file_name().expect("filename").to_str().unwrap(),
                        ),
                    length: "".to_string(), // todo actually calculate length
                    mime_type: MIME_TYPE.to_string(),
                }))
                .itunes_ext(rss::extension::itunes::ITunesItemExtensionBuilder::default().build())
                .build(),
        );
    }

    let channel = ChannelBuilder::default()
        .title(TITLE)
        .description("An RSS feed.")
        .items(items)
        .itunes_ext(rss::extension::itunes::ITunesChannelExtensionBuilder::default().build())
        .build();

    let mut file = fs::File::create(OUTPUT_FILE).expect("Failed to create output file");
    file.write_all(channel.to_string().as_bytes())
        .expect("Failed writing to file");
    println!("{}", channel.to_string());
}

fn vec_of_extension_match() -> Vec<std::path::PathBuf> {
    let mut vec = Vec::new();
    let paths = fs::read_dir("./").unwrap();

    for path in paths {
        match path.as_ref().unwrap().path().extension() {
            Some(val) => {
                if val.to_str().expect("REASON").to_lowercase() == EXTENSION {
                    vec.push(path.unwrap().path().to_path_buf());
                }
            }
            None => {}
        }
    }
    return vec;
}
